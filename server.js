const express = require("express");
const app = express(); 
const port = 3000;

app.get('/', (req, res) => {
    res.send('Hello World - AWS Study on EC2'); 
})

app.listen(port, () => {
    console.log(`
      ###########################################
        🔥 [AWS-STUDY] 🔥
        Server start!!!
        Listening on port: 3000
      ###########################################
    `);
  });
